# NodeJs

## Exercice: Is there a problem? _(1 points)_

```javascript
// Call web service and return count user, (got is library to call url)
async function getCountUsers() {
  return { total: await got.get('https://my-webservice.moveecar.com/users/count') };
}

// Add total from service with 20
async function computeResult() {
  const result = getCountUsers();
  return result.total + 20;
}
```
## Solution:

/* First Part:
* The issue in your Node.js code is that the 
* computeResult function doesn't wait for the
* 'getCountUsers' function to complete before 
* trying to access result.total.
 */

 /* Second Part:
* In this corrected code, the computeResult 
* function now uses await to wait for getCountUsers 
* to complete before accessing result.total. 
* Additionally, error handling has been added using 
* .catch in case any errors occur in the asynchronous functions.
 */


```javascript
const got = require('got');

async function getCountUsers() {
  const response = await got.get('https://my-webservice.moveecar.com/users/count');
  return { total: JSON.parse(response.body).total }; 
  // Parse the body of the JSON response if necessary
}

// Adding total from service with 20
async function computeResult() {
  const result = await getCountUsers();
  return result.total + 20;
}

// Call to the Asynchronous function
computeResult().then((finalResult) => {
  console.log('Final Result:', finalResult);
}).catch((error) => {
  console.error('Error:', error);
});

```


## Exercice: Is there a problem? _(2 points)_

```javascript
// Call web service and return total vehicles, (got is library to call url)
async function getTotalVehicles() {
    return await got.get('https://my-webservice.moveecar.com/vehicles/total');
}

function getPlurial() {
    let total;
    getTotalVehicles().then(r => total = r);
    if (total <= 0) {
        return 'none';
    }
    if (total <= 10) {
        return 'few';
    }
    return 'many';
}
```
## Solution:

/* First Part:
* The issue lies in the asynchronous nature of
*  the getTotalVehicles function. When you call 
* getTotalVehicles().then(r => total = r), 
* the code immediately continues to the next lines 
* without waiting for the promise to resolve.
 */

 /* Second Part:
* In this corrected code, the getPlural function 
* is also marked as async, and await is used with 
* getTotalVehicles() to ensure that the program waits 
* for the total count before proceeding to the conditional checks.
 */


```javascript
const got = require('got');

async function getTotalVehicles() {
    const response = await got.get('https://my-webservice.moveecar.com/vehicles/total');
    const responseBody = JSON.parse(response.body); // Assuming the response body is JSON
    return responseBody.total; // Assuming the total count is in the 'total' property
}

async function getPlurial() {
    try {
        const total = await getTotalVehicles();

        if (total <= 0) {
            return 'none';
        }
        if (total <= 10) {
            return 'few'; 
        }
        return 'many';
    } catch (error) {
        console.error('Error:', error);
        return 'error';
    }
}
```

## Exercice: Unit test _(2 points)_

Write unit tests in jest for the function below in typescript

```typescript
import { expect, test } from '@jest/globals';

function getCapitalizeFirstWord(name: string): string {
  if (name == null) {
    throw new Error('Failed to capitalize first word with null');
  }
  if (!name) {
    return name;
  }
  return name.split(' ').map(
    n => n.length > 1 ? (n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase()) : n
  ).join(' ');
}

test('1. test', async function () {
    ...
});
```
## solution:

```typescript
import { expect, test } from '@jest/globals';
import { getCapitalizeFirstWord } from './your-module-path'; // Replace with the correct path to your module

test('capitalize first word', async () => {
  // Test case 1: Capitalizing a single word
  expect(getCapitalizeFirstWord('hello')).toBe('Hello');

  // Test case 2: Capitalizing each word in a sentence
  expect(getCapitalizeFirstWord('hello world')).toBe('Hello World');

  // Test case 3: Handling null input
  expect(() => getCapitalizeFirstWord(null)).toThrowError('Failed to capitalize first word with null');

  // Test case 4: Handling empty input
  expect(getCapitalizeFirstWord('')).toBe('');

  // Test case 5: Handling single-character words
  expect(getCapitalizeFirstWord('a b c')).toBe('A B C');
});
```

# Angular

## Exercice: Is there a problem and improve the code _(5 points)_

```typescript
@Component({
  selector: 'app-users',
  template: `
    <input type="text" [(ngModel)]="query" (ngModelChange)="querySubject.next($event)">
    <div *ngFor="let user of users">
        {{ user.email }}
    </div>
  `
})
export class AppUsers implements OnInit {

  query = '';
  querySubject = new Subject<string>();

  users: { email: string; }[] = [];

  constructor(
    private userService: UserService
  ) {
  }

  ngOnInit(): void {
    concat(
      of(this.query),
      this.querySubject.asObservable()
    ).pipe(
      concatMap(q =>
        timer(0, 60000).pipe(
          this.userService.findUsers(q)
        )
      )
    ).subscribe({
      next: (res) => this.users = res
    });
  }
}
```
## Solution

/* Potential issues:
* Memory Leaks and Unsubscribing
* NgModelChange vs ValueChanges
* Timer for Polling
* 
* In this version, I replaced (ngModel) with a reactive form using FormControl and utilized the 
* valueChanges observable. Additionally, I added proper unsubscribing to prevent memory leaks 
* and simplified the code structure.
 */

```typescript

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, timer } from 'rxjs';
import { takeUntil, switchMap, debounceTime } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { UserService } from 'path-to-your-user-service';

@Component({
  selector: 'app-users',
  template: `
    <input type="text" [formControl]="queryControl">
    <button (click)="search()">Search</button>
    <div *ngFor="let user of users">
      {{ user.email }}
    </div>
  `
})
export class AppUsers implements OnInit, OnDestroy {
  queryControl = new FormControl('');
  private destroy$ = new Subject<void>();

  users: { email: string }[] = [];

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.queryControl.valueChanges
      .pipe(
        debounceTime(300), // Debounce to avoid frequent requests on each keystroke
        takeUntil(this.destroy$),
        switchMap(q => this.userService.findUsers(q))
      )
      .subscribe({
        next: (res) => (this.users = res),
        error: (err) => console.error('Error fetching users:', err)
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  search(): void {
    // Trigger the search manually when the button is clicked
    this.queryControl.setValue(this.queryControl.value);
  }
}
```

## Exercice: Improve performance _(5 points)_

```typescript
@Component({
  selector: 'app-users',
  template: `
    <div *ngFor="let user of users">
        {{ getCapitalizeFirstWord(user.name) }}
    </div>
  `
})
export class AppUsers {

  @Input()
  users: { name: string; }[];

  constructor() {}
  
  getCapitalizeFirstWord(name: string): string {
    return name.split(' ').map(n => n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase()).join(' ');
  }
}
```
## Solution

/* Refactor:
* To improve performance in the given code, you can leverage 
* Angular's Pipe to handle the transformation of user names. Additionally, 
* you can make the getCapitalizeFirstWord function a pure function to avoid 
* unnecessary recalculations. 
* 
* Here's an updated version of the component:
 */

```typescript

import { CapitalizeFirstWordPipe } from './capitalize-first-word.pipe'; // Create a pipe file

@Component({
  selector: 'app-users',
  template: `
    <div *ngFor="let user of users">
      {{ user.name | capitalizeFirstWord }}
    </div>
  `,
  providers: [CapitalizeFirstWordPipe] // Declare the pipe in the providers array
})
export class AppUsers {
  @Input()
  users: { name: string }[] = [];

  constructor() {}
}

/*  then, we need to create the CapitalizeFirstWordPipe
*/
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizeFirstWord'
})
export class CapitalizeFirstWordPipe implements PipeTransform {
  transform(value: string): string {
    if (!value) return value;

    return value
      .split(' ')
      .map(n => n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase())
      .join(' ');
  }
}

/* Angular's built-in pipe infrastructure is utilized for the transformation.
* The logic is encapsulated in a reusable CapitalizeFirstWordPipe.
* The getCapitalizeFirstWord function is now a pure function, and the pipe ensures
* that the transformation is only applied when necessary.
* Using Angular pipes in this way helps with performance optimizations and keeps your components cleaner and more maintainable.
*/ 
```

## Exercice: Forms _(8 points)_

Complete and modify `AppUserForm` class to use Angular Reactive Forms. Add a button to submit.

The form should return data in this format

```typescript
{
  email: string; // mandatory, must be a email
  name: string; // mandatory, max 128 characters
  birthday?: Date; // Not mandatory, must be less than today
  address: { // mandatory
    zip: number; // mandatory
    city: string; // mandatory, must contains only alpha uppercase and lower and space
  };
}
```

```typescript
@Component({
  selector: 'app-user-form',
  template: `
    <form>
        <input type="text" placeholder="email">
        <input type="text" placeholder="name">
        <input type="date" placeholder="birthday">
        <input type="number" placeholder="zip">
        <input type="text" placeholder="city">
    </form>
  `
})
export class AppUserForm {

  @Output()
  event = new EventEmitter<{ email: string; name: string; birthday: Date; address: { zip: number; city: string; };}>;
  
  constructor(
    private formBuilder: FormBuilder
  ) {
  }

  doSubmit(): void {
    this.event.emit(...);
  }
}
```

## Solution

```typescript

import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  template: `
    <form [formGroup]="userForm" (ngSubmit)="doSubmit()">
      <input type="text" placeholder="Email" formControlName="email">
      <div *ngIf="userForm.get('email').hasError('email') && userForm.get('email').touched">
        Please enter a valid email.
      </div>

      <input type="text" placeholder="Name" formControlName="name">
      <div *ngIf="userForm.get('name').hasError('maxlength') && userForm.get('name').touched">
        Name should be at most 128 characters.
      </div>

      <input type="date" placeholder="Birthday" formControlName="birthday">
      <div *ngIf="userForm.get('birthday').hasError('dateInvalid') && userForm.get('birthday').touched">
        Please enter a valid date (less than today).
      </div>

      <input type="number" placeholder="ZIP" formControlName="zip">
      <input type="text" placeholder="City" formControlName="city">

      <button type="submit" [disabled]="userForm.invalid">Submit</button>
    </form>
  `
})
export class AppUserForm {
  @Output() event = new EventEmitter<{
    email: string;
    name: string;
    birthday: Date;
    address: { zip: number; city: string };
  }>();

  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.userForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required, Validators.maxLength(128)]],
      birthday: ['', this.validateDate],
      zip: ['', Validators.required],
      city: ['', [Validators.required, Validators.pattern(/^[a-zA-Z ]*$/)]]
    });
  }

  doSubmit(): void {
    if (this.userForm.valid) {
      const formData = this.userForm.value;
      this.event.emit(formData);
      this.userForm.reset();
    }
  }

  validateDate(control) {
    const selectedDate = new Date(control.value);
    const today = new Date();
    return selectedDate <= today ? null : { dateInvalid: true };
  }
}
```

# CSS & Bootstrap

## Solution
```HTML

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Card Exercice</title>
  <style>
    .card {
      width: 600px;
      height: 200px;
      background-color: rgba(255, 255, 255, 0.993);
      border: 2px solid #d3d3d3e3;
      border-radius: 5px;
      box-shadow: 0 3px 1px rgba(104, 103, 103, 0.048);
      padding: 20px;
      position: relative;
      margin:auto;
      top:200px; 
    }

    .title {
      font-size: 28px;
      font-weight: bold;
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
      margin-bottom: 15px;
      color: #2a2a2a;
    }

    .paragraph {
      font-size: 24px;
      margin-bottom: 15px;
      font-weight:500;
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    }

    .buttons-container {
      position: absolute;
      bottom: 25px;
      right: 25px;
    }

    .button {
      padding: 15px;
      font-size: 26px;      
    }

    .got-it {
      width: 100px;
      padding-right: 10;
      background-color: rgb(26, 26, 26);
      color:antiquewhite;
      border-radius: 7px 0px 0px 7px;
      margin-right: -5px; 
    }

    .dont-know {
      width: 220px;
      border: 2px solid #2a2a2a;
      border-radius: 0px 7px 7px 0px;     
    }

    .options-dropdown {
      padding: 15px;
      width: 160px;
      margin-left: 5px;
      border-radius: 7px;
      background-color: #5c5c5c;
      color: rgb(241, 241, 241);
      appearance: none;
    }

    .options-dropdown::after {
      content: '';
      position: absolute;
      top: 50%;
      right: 10px;
      transform: translateY(-50%);
      border-style: solid;
      border-width: 6px 6px 0;
      border-color: rgb(241, 241, 241) transparent transparent; /* Arrow color */
    }

    .badge {
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
      font-weight: bolder;
      font-stretch: narrower;
      font-size: 18px;
      position: absolute;
      top: -18px;
      right: -35px;
      background-color: rgb(255, 67, 67);
      color: white;
      padding: 5px 15px;
      border-radius: 20px 20px 20px 20px;
    }
  </style>
</head>
<body>
  <div class="card">
    <div class="badge">+123</div>
    <div class="title">Exercice</div>
    <div class="paragraph">Redo this card in CSS and if possible using Bootstrap 4 or 5</div>
    <div class="buttons-container">
      <button class="button got-it">Got it</button>
      <button class="button dont-know">I don't Know</button>
      <select class="button options-dropdown">
        <option value="options">Options</option>
        <!-- Agrega las opciones deseadas dentro del desplegable -->
      </select>
    </div>
  </div>
</body>
</html>
```



## Exercice: Card _(5 points)_

![image](uploads/0388377207d10f8732e1d64623a255b6/image.png)

# MongoDb

## Exercice: MongoDb request _(3 points)_

MongoDb collection `users` with schema

``` typescript
  {
    email: string;
    first_name: string;
    last_name: string;
    roles: string[];
    last_connection_date: Date;
  }
```

Complete the query, you have a variable that contains a piece of text to search for. Search by exact email, starts with first or last name and only users logged in for 6 months

``` typescript
db.collections('users').find(...);
```
## Solution 

```typescript 
const searchText = "example"; // Replace with the actual text to search for
const targetRoles = ["user", "admin"]; // Replace with the roles you want to search for

db.users.find({
  $and: [
    {
      $or: [
        { email: searchText },
        { first_name: { $regex: `^${searchText}`, $options: 'i' } },
        { last_name: { $regex: `^${searchText}`, $options: 'i' } }
      ]
    },
    { last_connection_date: { $gte: new Date(new Date().setMonth(new Date().getMonth() - 6)) } },
    { roles: { $in: targetRoles } } // Check if the user has any of the specified roles
  ]
});
```

What should be added to the collection so that the query is not slow?

```typescript
/* Creates index in the ascending order (1) // or descending order (-1)
*/
db.users.createIndex({ email: 1 });
db.users.createIndex({ first_name: 1 });
db.users.createIndex({ last_name: 1 });
db.users.createIndex({ last_connection_date: 1 });
```

## Exercice: MongoDb aggregate _(5 points)_

MongoDb collection `users` with schema

``` typescript
  {
    email: string;
    first_name: string;
    last_name: string;
    roles: string[];
    last_connection_date: Date;
  }
```

Complete the aggregation so that it sends user emails by role ({_id: 'role', users: [email,...]})

``` typescript
db.collections('users').aggregate(...);
```
## solution

/* To aggregate and group users by role and send user emails
 * for each role. Here's an example aggregation pipeline that
 * groups users by role and collects their emails:
*/ 

```typescript

import { MongoClient, Db } from 'mongodb';

interface User {
  email: string;
  first_name: string;
  last_name: string;
  roles: string[];
  last_connection_date: Date;
}

async function getUsersByRole() {
  const targetRoles = ["user", "admin"];

  const client = new MongoClient('mongodb://your-mongodb-connection-string', { useNewUrlParser: true, useUnifiedTopology: true });

  try {
    await client.connect();

    const database: Db = client.db('your-database-name');
    const usersCollection = database.collection<User>('users');

    const aggregationPipeline = [
      {
        $match: {
          roles: { $in: targetRoles }
        }
      },
      {
        $group: {
          _id: '$roles',
          emails: { $push: '$email' }
        }
      }
    ];

    const result = await usersCollection.aggregate(aggregationPipeline).toArray();

    console.log(result);
  } finally {
    await client.close();
  }
}

```
## Exercice: MongoDb update _(5 points)_

MongoDb collection `users` with schema

``` typescript
  {
    email: string;
    first_name: string;
    last_name: string;
    roles: string[];
    last_connection_date: Date;
    addresses: {
        zip: number;
        city: string;
    }[]:
  }
```

Update document `ObjectId("5cd96d3ed5d3e20029627d4a")`, modify only `last_connection_date` with current date

``` typescript
db.collections('users').updateOne(...);
```
## Solution `last_conection_date`

```typescript
import mongoose, { Document, Schema } from 'mongoose';

interface Address {
  zip: number;
  city: string;
}

interface User extends Document {
  email: string;
  first_name: string;
  last_name: string;
  roles: string[];
  last_connection_date: Date;
  addresses: Address[];
}

const userSchema = new Schema<User>({
  email: { type: String, required: true },
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  roles: { type: [String], required: true },
  last_connection_date: { type: Date, required: true },
  addresses: [{ zip: Number, city: String }],
});

const UserModel = mongoose.model<User>('user', userSchema);

async function updateLastConnectionDate(objectId: string) {
  try {
    await mongoose.connect('mongodb://your-mongodb-connection-string', { useNewUrlParser: true, useUnifiedTopology: true });

    const userIdObject = mongoose.Types.ObjectId(objectId);

    const result = await UserModel.updateOne(
      { _id: userIdObject },
      { $set: { last_connection_date: new Date() } }
    );

  } finally {
    await mongoose.disconnect();
  }
}

const objectIdToUpdate = '5cd96d3ed5d3e20029627d4a'; // Replace with the actual ObjectId
updateLastConnectionDate(objectIdToUpdate);
```

Update document `ObjectId("5cd96d3ed5d3e20029627d4a")`, add a role `admin`

``` typescript
db.collections('users').updateOne(...);
```
## Solution add a role `admin`

```typescript
import mongoose, { Document, Schema } from 'mongoose';

interface Address {
  zip: number;
  city: string;
}

interface User extends Document {
  email: string;
  first_name: string;
  last_name: string;
  roles: string[];
  last_connection_date: Date;
  addresses: Address[];
}

const userSchema = new Schema<User>({
  email: { type: String, required: true },
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  roles: { type: [String], required: true },
  last_connection_date: { type: Date, required: true },
  addresses: [{ zip: Number, city: String }],
});

const UserModel = mongoose.model<User>('User', userSchema);

async function addAdminRole(objectId: string) {
  try {
    if (mongoose.connection.readyState === 0) {
      await mongoose.connect('mongodb://your-mongodb-connection-string', { useNewUrlParser: true, useUnifiedTopology: true });
    }

    const userIdObject = mongoose.Types.ObjectId(objectId);

    const result = await UserModel.updateOne(
      { _id: userIdObject },
      { $addToSet: { roles: 'admin' } } // Use $addToSet to add the role only if it doesn't already exist
    );

  } catch (error) {
    console.error('Error adding admin role:', error);
  } finally {
    if (mongoose.connection.readyState === 1) {
      await mongoose.disconnect();
    }
  }
}

const objectIdToUpdate = '5cd96d3ed5d3e20029627d4a'; // Replace with the actual ObjectId
addAdminRole(objectIdToUpdate);

```
Update document `ObjectId("5cd96d3ed5d3e20029627d4a")`, modify addresses with zip `75001` and replace city with `Paris 1`

``` typescript
db.collections('users').updateOne(...);
```
## Solution modify addresses with zip `75001` and replace city with `Paris 1`

```typescript
import mongoose, { Document, Schema } from 'mongoose';

interface Address {
  zip: number;
  city: string;
}

interface User extends Document {
  email: string;
  first_name: string;
  last_name: string;
  roles: string[];
  last_connection_date: Date;
  addresses: Address[];
}

const userSchema = new Schema<User>({
  email: { type: String, required: true },
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  roles: { type: [String], required: true },
  last_connection_date: { type: Date, required: true },
  addresses: [{ zip: Number, city: String }],
});

const UserModel = mongoose.model<User>('User', userSchema);

async function modifyAddress(objectId: string, zip: number, newCity: string) {
  try {
    if (mongoose.connection.readyState === 0) {
      await mongoose.connect('mongodb://your-mongodb-connection-string', { useNewUrlParser: true, useUnifiedTopology: true });
    }

    const userIdObject = mongoose.Types.ObjectId(objectId);

    const result = await UserModel.updateOne(
      { _id: userIdObject, 'addresses.zip': zip },
      { $set: { 'addresses.$.city': newCity } }
    );

    console.log(`Matched ${result.n} document(s) and modified ${result.nModified} document(s)`);
  } catch (error) {
    console.error('Error modifying address:', error);
  } finally {
    if (mongoose.connection.readyState === 1) {
      await mongoose.disconnect();
    }
  }
}

const objectIdToUpdate = '5cd96d3ed5d3e20029627d4a'; // Replace with the actual ObjectId
const zipCode = 75001;
const newCity = 'Paris 1';

modifyAddress(objectIdToUpdate, zipCode, newCity);
```